import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:vclass/AddClass.dart';
import 'package:vclass/ClassData.dart';
import 'package:vclass/Schedule.dart';

import 'DbConnect.dart';
import 'model/Classes.dart';

class ShowClasses extends StatelessWidget {
  final String classname;
  ShowClasses({Key key, @required this.classname}) : super(key: key);
  final myController = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  DbConnect dbConnect=DbConnect.instance;
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: FittedBox(
              child: Text(
                  "Edit or Delete a Class",
                textAlign: TextAlign.center,
              ),
            )
        ),
     body: FutureBuilder(
       future: dbConnect.fetchClasses(),
         builder: (context, projectSnap) {
           if (!projectSnap.hasData) {
             return Center(
               child: Container(
                 margin: const EdgeInsets.all(10.0),
                 color: Colors.blue[200],
                 width: MediaQuery. of(context). size. width*8/10,
                 height:MediaQuery. of(context). size. width*8/10,
                 child: Text(
                     "\n\nSeems You Have Not Created a Class yet..\nNo Worries :)\nStart Managing your Classes with VClass !",
                     textAlign: TextAlign.center,
                     style: TextStyle(
                       fontWeight: FontWeight.bold,
                       fontSize: 18.5,
                       color: Colors.white,
                     )
                 ),
               ),
             );
           }
           List <Classes> classlist=projectSnap.data;
           return Container(
            // child: Text("listt : "+classlist[1].classname),
             child: GridView.count(
               primary: false,
               padding: const EdgeInsets.all(20),
               crossAxisSpacing: 10,
               mainAxisSpacing: 10,
               crossAxisCount: 2,
               children: List.generate(classlist.length, (index){
                 return Container(
                     padding: const EdgeInsets.all(8),
                     child: ListTile(
                       tileColor: Colors.blue,
                      // trailing:Icon(Icons.group_rounded),
                       subtitle: Text(
                         (classlist[index].numberlist.split(",").length-1)==1?"1 student":
                         (classlist[index].numberlist.split(",").length-1).toString()+" students",
                           textAlign: TextAlign.left,
                           style: TextStyle(
                            //fontWeight: FontWeight.bold,
                            fontSize: 18.5,
                            color: Colors.white,

                         ),) ,
                       title: Text(
                         classlist[index].classname+"\n",
                         textAlign: TextAlign.center,
                         //overflow: TextOverflow.ellipsis,
                         style: TextStyle(
                             fontWeight: FontWeight.bold,
                             fontSize: 30.5,
                             color: Colors.white,

                         ),
                       ),
                       onTap:(){
                         myController.text=classlist[index].classname.toString();
                         print(myController.text);
                         Navigator.push(
                           context,
                           MaterialPageRoute(builder: (context) =>ClassData(classname:myController.text,clzID: classlist[index].id,listlength: classlist[index].namelist.split(",").length )),
                         );
                       },
                     ),
                   //  color:index==4||index==9?Colors.blueGrey:Colors.cyan[(index+1)*200%1000],
                   color:Colors.cyan[(index+1)*200%1000]==Colors.white||Colors.cyan[(index+1)*200%1000]==null?Colors.blueGrey:Colors.cyan[(index+1)*200%1000],
                 );
               }),
             ),
           );
         }
     ),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            backgroundColor: Colors.blue[900],
            foregroundColor: Colors.white,
            heroTag: "btn2",
            onPressed: () =>
            {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => AddClass()),
              )
            })
    );
  }

}