import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:vclass/DbConnect.dart';
import 'package:vclass/model/Payments.dart';

class PaymentHistory extends StatefulWidget {
  final String classid;
  final String name;
  final String number;
  final String months;


  PaymentHistory(
      {Key key, @required this.classid, @required this.name, @required this.number, @required this.months})
      : super(key: key);

  @override
  PaymentHistoryState createState() =>new PaymentHistoryState(classid: classid,name: name,number: number,months: months);
}
class PaymentHistoryState extends State <PaymentHistory>{

  @override
  void dispose() {
    super.dispose();
  }

  final String classid;
  final String name;
  final String number;
  String months;
  bool is_paymentupdated=false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  PaymentHistoryState(
      {Key key, @required this.classid, @required this.name, @required this.number, @required this.months});

  DbConnect dbConnect=DbConnect.instance;
  List <String> monthofyear=["January","February","March","April","May","June","July","August","September","October","November","December"];

  Widget build(BuildContext context) {
    List<String> monthpay=this.months.split(",");
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          title: FittedBox(child: Text(classid + " : Payment History"))
      ),
      body: Container(
          margin: const EdgeInsets.all(10.0),
          //color: Colors.cyan[600],
          //width: MediaQuery. of(context). size. width*9/10,
          //  height:MediaQuery. of(context). size. height*9/10,
          child: FutureBuilder(
              future: dbConnect.getPaymentData(classid),
              builder: (context, projectSnap) {
                if (!projectSnap.hasData) {
                  return Center(
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      color: Colors.cyan,
                      width: MediaQuery
                          .of(context)
                          .size
                          .width * 8 / 10,
                      height: MediaQuery
                          .of(context)
                          .size
                          .width * 8 / 10,
                      child: Text(
                          "\n\noops! Something went wrong :(\nPlease try again)",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.5,
                            color: Colors.white,
                          )
                      ),
                    ),
                  );
                }
                List <Payments> paymentdata = projectSnap.data;
                return Container(
                  child: ListView(
                    children: [
                      ListTile(
                        title: SizedBox(
                          width: MediaQuery
                              .of(context)
                              .size
                              .width * 9 / 10,
                          height: MediaQuery
                              .of(context)
                              .size
                              .height * 0.5 / 10,
                          child: FittedBox(
                            child: Text(
                              name+"'s Payments",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 30.0,
                                  //  backgroundColor: Colors.blue,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white
                              ),
                            ),
                          ),
                        ),
                        tileColor: Colors.cyan[500],
                      ),
                      for(int y=0;y<monthofyear.length;y++)
                        ListTile(
                          contentPadding: EdgeInsets.all(5.0),
                            selectedTileColor: Colors.lightBlue,
                            leading: Icon(Icons.payment),
                            title: Text(
                                monthofyear.elementAt(y)),
                            subtitle: Text(
                                monthpay.elementAt(y)=="n"?"Not Paid":"Paid",
                            ),
                            trailing: FloatingActionButton.extended(
                                label:Text("Change"),
                              heroTag: "btn1",
                              onPressed:(){
                                Alert(
                                  closeIcon: Icon(Icons.close),
                                  context: context,
                                  type: AlertType.info,
                                  title: "Update "+name+"'s Payment as",
                                  desc: " "+(monthpay.elementAt(y)=="n"? "Paid":"Not Paid")+" for "+monthofyear.elementAt(y)+"?",
                                  buttons: [
                                    DialogButton(
                                      child: Text(
                                        "Yes",
                                        style: TextStyle(color: Colors.white, fontSize: 20),
                                      ),
                                      onPressed: () {
                                        updatePaydata(y,monthpay.elementAt(y));
                                        Navigator.of(context, rootNavigator: true).pop();
                                        _showToast(_scaffoldKey.currentContext);
                                      },
                                      width: 120,
                                    ),
                                    DialogButton(
                                      child: Text(
                                        "No",
                                        style: TextStyle(color: Colors.white, fontSize: 20),
                                      ),
                                      //onPressed: () => Navigator.pop(context),
                                      onPressed: ()=>Navigator.of(context, rootNavigator: true).pop(),
                                      width: 120,
                                    )
                                  ],
                                ).show();
                              },
                            ), //onTapped(x.phones.first.value),
                          onTap:(){var selected=true;},// tileColor:selected?Colors.red:Colors.transparent,
                        ),

                    ],
                  ),

                );
              }
          )

      ),
    );
  }

  void updatePaydata(int index, String paydatabeforechange) {
    List<String> monthlypayments=this.months.split(",");
    String newvalue;
    String monthsvalue="";
    if(paydatabeforechange=="y") {
      newvalue="n";
    }else{
      newvalue="y";
    }
    monthlypayments[index] = newvalue;

    for(var x in monthlypayments){
      monthsvalue=monthsvalue+","+x;
    }
    print(" monthsvalue :"+monthsvalue);
    monthsvalue=monthsvalue.substring(1,24);
    print("new monthsvalue :"+monthsvalue);

    print("before update :"+classid+" "+number+" "+name+" "+monthsvalue);
    Payments payment=new Payments(classname: classid,number: number,name: name,months:monthsvalue );
    var val= dbConnect.updatePayment(payment);

    if(val!=null&&mounted) {//incorrect here.. need to check whether val>0
      setState(() {
        months = monthsvalue;
        is_paymentupdated=true;
      });
    }else if(mounted){
     setState(() {
       is_paymentupdated=false;
      });
    }
   // print("val "+val.toString());
    }

  void _showToast(BuildContext context) {
    final scaffold = _scaffoldKey.currentState;
    String message="";
    if(is_paymentupdated==true){
      message= "Payment Update Success!";
    }else{
      message="Oops!Something went wrong";
    }
     scaffold.showSnackBar(
      SnackBar(
        content: Text(message),
        action: SnackBarAction(
            label: 'Hide', onPressed: scaffold.hideCurrentSnackBar),
        backgroundColor: is_paymentupdated?Colors.green:Colors.redAccent,
      ),
    );
  }

}