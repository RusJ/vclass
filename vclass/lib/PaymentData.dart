import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vclass/DbConnect.dart';
import 'package:vclass/PaymentHistory.dart';
import 'package:vclass/model/Payments.dart';

class PaymentData extends StatelessWidget{
  final String classid;
  var now = new DateTime. now();
  var formatter = new DateFormat('MM');

  DbConnect dbConnect=DbConnect.instance;
  PaymentData({Key key, @required this.classid}) : super(key: key);


  Widget build(BuildContext context) {
    String curmonth = formatter. format(now);
    int currmonthnum=int.parse(curmonth);
    return Scaffold(
      appBar: AppBar(
          title:FittedBox(child:Text(classid+" : Payment data"))
      ),
      body: Container(
          margin: const EdgeInsets.all(10.0),
          //color: Colors.cyan[600],
          //width: MediaQuery. of(context). size. width*9/10,
          //  height:MediaQuery. of(context). size. height*9/10,
          child: FutureBuilder(
              future: dbConnect.getPaymentData(classid),
              builder: (context, projectSnap) {
                if (!projectSnap.hasData) {
                  return Center(
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      color: Colors.cyan,
                      width: MediaQuery. of(context). size. width*8/10,
                      height:MediaQuery. of(context). size. width*8/10,
                      child: Text(
                          "\n\noops! Something went wrong :(\nPlease try again)",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.5,
                            color: Colors.white,
                          )
                      ),
                    ),
                  );
                }
                List <Payments> paymentdata=projectSnap.data;
                return Container(
                  child: ListView(
                    children: [
                      ListTile(
                        title:SizedBox(
                          width:MediaQuery. of(context). size. width*9/10,
                          height: MediaQuery. of(context). size.height*0.5/10,
                          child: FittedBox(
                            child: Text(
                              "Select on student to view payments",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 30.0,
                                  //  backgroundColor: Colors.blue,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white
                              ),
                            ),
                          ),
                        ),
                        tileColor: Colors.cyan,
                      ),
                      for(var x in paymentdata)
                        Container(
                          margin: EdgeInsets.all(6.0),
                          child: ListTile(
                            leading: Icon(Icons.people),
                            title: Text(
                                x.name),
                            subtitle: Text(
                               x.number
                            ),
                            tileColor: Colors.lightBlue[200],
                            /*trailing: Icon(
                              x.months.split(",").elementAt(currmonthnum-1)=="y"?Icons.check_box:Icons.check_box_outline_blank,
                              color: Colors.blue.shade400,
                            ),*/
                            onTap:(){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) =>PaymentHistory(classid:classid,name: x.name,number: x.number,months: x.months,)),
                              );
                            },//onTapped(x.phones.first.value),
                            // tileColor:selected?Colors.red:Colors.transparent,
                          ),
                        ),

                    ],
                  ),

                );
              }
          )

      ),
    );
   /* return Scaffold(
      appBar: AppBar(
          title: Text("Payment Details")
      ),
      body: Center(
        child: Container(
            margin: const EdgeInsets.all(10.0),
            color: Colors.cyan[600],
            width: MediaQuery. of(context). size. width*9/10,
            height:MediaQuery. of(context). size. height*9/10,
            child: FutureBuilder(
                future: dbConnect.getPaymentData(classid),
                builder: (context, projectSnap) {
                  if (!projectSnap.hasData) {
                    return Center(
                      child: Container(
                        margin: const EdgeInsets.all(10.0),
                        color: Colors.cyan,
                        width: MediaQuery. of(context). size. width*8/10,
                        height:MediaQuery. of(context). size. width*8/10,
                        child: Text(
                            "\n\noops! Something went wrong :(\nPlease try again)",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.5,
                              color: Colors.white,
                            )
                        ),
                      ),
                    );
                  }
                  List <Payments> paymentdata=projectSnap.data;
                  return Container(
                    child: ListTile(
                      title: Text(
                        "Class Name : "+paymentdata[0].classname,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 30.0,
                            //  backgroundColor: Colors.blue,
                            fontWeight: FontWeight.bold,
                            color: Colors.white
                        ),
                      ),
                      subtitle: Text("Students",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white
                        ),
                      ),
                    ),
                  );
                }
            )
        ),
      ),
    );*/
  }
}