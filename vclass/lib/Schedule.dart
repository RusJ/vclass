import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

import 'AddEvent.dart';
import 'DbConnect.dart';
import 'Meetings.dart';
import 'MeetingDataSource.dart';
import 'model/Meeting.dart';
class Schedule extends StatefulWidget {
  @override
  ScheduleState createState() => new ScheduleState();
}
class ScheduleState extends State<Schedule>{

  @override
  void initState() {
    super.initState();
    extractFuture();
  }

  DbConnect dbConnect=DbConnect.instance;
  String eventname="";
  var eventlist;
  var meetinglist;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            Expanded(
              child: Container(
               // height: MediaQuery.of(context).size.height * 0.65,
                child: SfCalendar(
                  view: CalendarView.month,
                  dataSource: MeetingDataSource(_getDataSource()),
                  monthViewSettings: MonthViewSettings(
                      showAgenda: true,
                      appointmentDisplayMode: MonthAppointmentDisplayMode.appointment,
                  ),
                ),
              ),
            ),
            Container(
              child: FloatingActionButton.extended(
                  onPressed:()=>{
                    Navigator.push(context,
                        MaterialPageRoute(
                            builder: (context) => AddEvent()
                        )
                    )
                  },
                  label: Text("Add Event"),
                heroTag: "btn3",
              ),
              padding: EdgeInsets.all(15.0),
            )
          ],
        )
    );
  }
  List<Meetings> _getDataSource() {
    var meetings = <Meetings>[];
    /*
    final DateTime today = DateTime.now();
    final DateTime startTime =
    DateTime(today.year, today.month, today.day, 9, 0, 0);
    print("\n\nstart time :  "+startTime.toString());
    final DateTime endTime = startTime.add(const Duration(hours: 2));
    meetings.add(
        Meetings('Conference1', startTime, endTime, const Color(0xFF0F8644), false));
    meetings.add(
        Meetings('Conference2', startTime, endTime, const Color(0xFF0F8644), false));
    meetings.add(
        Meetings('Conference3', startTime, endTime, const Color(0xFF0F8644), true));
*/
    if(meetinglist!=null) {
      for (var ev in meetinglist) {
        print(ev.eventname);
        var fromDateTimeObj = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(
            ev.fromdate + ":00.000000");
        var toDateTimeObj = new DateFormat("yyyy-MM-dd HH:mm:ss").parse(
            ev.todate + ":00.000000");

        print("---------parsed from #" + fromDateTimeObj.toString());

        meetings.add(
            Meetings(ev.eventname, fromDateTimeObj, toDateTimeObj,
                const Color(0xFF0F8644), false)
        );
      }
    }
    return meetings;
  }


  Future<void> extractFuture()async{
    eventlist= await dbConnect.fetchEvents();
    meetinglist=List<Meeting>(eventlist.length);
    try{
      int y=0;
      for(var x in eventlist) {
        setState(() {
          eventname = x.eventname+","+eventname;
          meetinglist[y]=x;
        });
        y++;
      }
    }catch(e){}
    print("String "+eventname);
  }

  /*List<Meetings> _getDataSource(){
    var meetings = <Meetings>[];
    Future eventlist=dbConnect.fetchEvents();
    eventlist.then((data) async{
      int y=0;
       for(var x in data) {

        var fromDateTimeObj =await new DateFormat("yyyy-MM-dd HH:mm:ss").parse(x.fromdate+":00.000000");
        var toDateTimeObj =await new DateFormat("yyyy-MM-dd HH:mm:ss").parse(x.todate+":00.000000");

        print("---------parsed from #"+fromDateTimeObj.toString());

        await meetings.add(
          await Meetings(x.eventname,fromDateTimeObj,toDateTimeObj,const Color(0xFF0F8644),false)
        );

        await meetings.isEmpty?print("empty meetings\n"):print("meeting added\n");
        ///////////////////////
        final DateTime today = DateTime.now();
        final DateTime startTime =
        DateTime(today.year, today.month, today.day, 9, 0, 0);
        print("\n\nstart time :  "+startTime.toString());
        final DateTime endTime = startTime.add(const Duration(hours: 2));
        await meetings.add(
           await  Meetings('Conference', fromDateTimeObj, toDateTimeObj, const Color(0xFF0F8644), false));
      ///////////////////////////////////

        //case from and todate Strings to DateTime meetings.add
      }
      return await meetings;
    }, onError: (e) {
      print("error "+e);
      //return null;
    });
    if(meetings.isEmpty){print("##empty");}
    for(var d in meetings){
      print("meetings####"+d.eventname);
    }
    return meetings;
  }*/

}