
import 'package:flutter/material.dart';
import 'package:contacts_service/contacts_service.dart';
class AddPeople extends StatelessWidget {
  var _isButtonEnabled=false;
  final _formKey = GlobalKey<FormState>();
  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
          title: Text("Create new Class")
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Form(
            key: _formKey,
            child :Column(
              children: <Widget>[

                TextFormField(
                  decoration: const InputDecoration(
                    icon: const Icon(Icons.school),
                    hintText: 'Enter the class name or ID',
                    labelText: 'Class Name',
                  ),
                  validator: (value) {
                    if (value.isEmpty) {
                      _isButtonEnabled=false;
                      return 'Please enter valid name';
                    }
                    _isButtonEnabled=true;
                    return null;
                  },
                ),

                ElevatedButton(
                  onPressed: () {
                    // Validate returns true if the form is valid, otherwise false.
                    if (_formKey.currentState.validate()) {
                      // If the form is valid, display a snackbar. In the real world,
                      // you'd often call a server or save the information in a database.
                      /*Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => AddStudents()),
                    );*/
                      //loadContacts();
                    }
                  },
                  child: Text('Add People'),
                ),
              ],
            ),
          ),
          Expanded(
            child: Column(
              children: [
                FutureBuilder(
                    future: loadContacts(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return CircularProgressIndicator();
                      } else {
                        Iterable<Contact> list = snapshot.data;
                        print(list
                            .elementAt(1)
                            .displayName);
                        return Expanded(
                          child: ListView(
                            children:
                            <Widget>[
                              for(var x in list)
                                ListTile(
                                  leading: Icon(Icons.people),
                                  title: Text(
                                      x.displayName
                                  ),
                                  subtitle: Text(
                                      x.phones.isEmpty ? "" : x.phones.first
                                          .value
                                  ),
                                  trailing: IconButton(
                                    icon: Icon(Icons.add_rounded),
                                    // onPressed: AddContact(),
                                  ),
                                  //selected: MaterialState.pressed != null?true:false,
                                ),
                              BottomAppBar(
                                child: Form(
                                  child: TextFormField(

                                  ),
                                ),
                              )
                            ],
                          ),
                        );
                      }
                    }),
              ],
            ),
          ),

        ],
      ),
    );
  }
  Future<Iterable<Contact>> loadContacts () async {
    print("function called");
    Iterable<Contact> contacts= await ContactsService.getContacts();
    for(var i=0;i<contacts.length;i++){
      print(contacts.elementAt(i).displayName);
    }
    /* Iterable<String> names = contacts.map((c) => c.displayName);
    Iterable<String> numbers = contacts.map((c) => c.phones.first.value);
    print(numbers.elementAt(4));

    List<Widget> list = new List<Widget>();
    for(var i = 0; i < contacts.length; i++){
      list.add(new Text(contacts.elementAt(i).displayName));
    }*/
    return contacts;
  }

  contactOnTapped(var cname) {
    print("Tapped on contact: "+cname);
  }


}