import 'package:flutter/material.dart';
import 'package:permissions_plugin/permissions_plugin.dart';
import 'Schedule.dart';
import 'Payments.dart';
import 'Notices.dart';
import 'ShowClasses.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    checkPermissions(context);
    return MaterialApp(
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            title: Text('VClass'),
            centerTitle: true,
            leading: Image.asset('icon/title_img.png'),
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.home), text: "Classes"),
                Tab(icon: Icon(Icons.message), text: "Notices"),
                Tab(icon: Icon(Icons.money),text: "Pay"),
                Tab(icon: Icon(Icons.calendar_today),text: "Calender")
              ],
            ),
          ),
          body: TabBarView(
            children: [
              ShowClasses(),
              Notices(),
              Payments(),
              Schedule(),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> checkPermissions(BuildContext context) async {

    Map<Permission, PermissionState> permission = await PermissionsPlugin
        .checkPermissions([
      Permission.READ_CONTACTS,
      Permission.WRITE_CONTACTS
    ]);

    if( permission[Permission.READ_CONTACTS] != PermissionState.GRANTED ||
        permission[Permission.WRITE_CONTACTS] != PermissionState.GRANTED) {

      try {
        permission = await PermissionsPlugin
            .requestPermissions([
          Permission.READ_CONTACTS,
          Permission.WRITE_CONTACTS
        ]);
      } on Exception {
        debugPrint("Error");
      }

      if( permission[Permission.READ_CONTACTS] == PermissionState.GRANTED &&
          permission[Permission.WRITE_CONTACTS] == PermissionState.GRANTED )
        print("Login ok");
      else
        permissionsDenied(context);

    } else {
      print("Login ok");
    }
  }

  void permissionsDenied(BuildContext context){
    showDialog(context: context, builder: (BuildContext _context) {
      return SimpleDialog(
        title: const Text("Permisos not Granted"),
        children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 30, right: 30, top: 15, bottom: 15),
            child: const Text(
              "Application may not be able to function s intended due to permission restrictions\nGo to app settings and give contact permission to go ahead! ",
              style: TextStyle(
                  fontSize: 18,
                  color: Colors.black54
              ),
            ),
          )
        ],
      );
    });
  }

}
