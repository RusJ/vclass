import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sms/flutter_sms.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:vclass/DbConnect.dart';
import 'package:vclass/model/Classes.dart';

class Messages extends StatefulWidget {
  final String classname;

  Messages({Key key, @required this.classname}) : super(key: key);
  @override
  MessagesState createState() => new MessagesState(classname:classname);

}

class MessagesState extends State<Messages>{

  final String classname;
  DbConnect dbConnect=DbConnect.instance;
  final _formKey = GlobalKey<FormState>();
  final messageController = TextEditingController();
  final showController = TextEditingController();
  bool addnotfromcontact=false;
  String addorcancelbtn="Add";
  String _selectedtype="Send to Paid";
  var _isButtonEnabled=false;

  final _formKey2 = GlobalKey<FormState>();
  final shownameController = TextEditingController();
  final nameController = TextEditingController();
  final numberController = TextEditingController();

  var now = new DateTime. now();
  var formatter = new DateFormat('MM');
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    showController.dispose();//remove if misbehave
    shownameController.dispose();
    nameController.dispose();
    numberController.dispose();
    super.dispose();
  }

  MessagesState({Key key, @required this.classname}) {
    var clzdata=dbConnect.getClassData(classname);
  }
  Widget build(BuildContext context) {
    String curmonth = formatter. format(now);
    int currmonthnum=int.parse(curmonth);
    return Scaffold(
      appBar: AppBar(
          title: Text("Class: "+classname)
      ),
      body: Container(
          margin: const EdgeInsets.all(10.0),
          //color: Colors.cyan[600],
          //width: MediaQuery. of(context). size. width*9/10,
          //  height:MediaQuery. of(context). size. height*9/10,
          child: FutureBuilder(
              future: dbConnect.getClassData(classname),
              builder: (context, projectSnap) {
                if (!projectSnap.hasData) {
                  return Center(
                    child: Container(
                      margin: const EdgeInsets.all(10.0),
                      color: Colors.cyan,
                      width: MediaQuery. of(context). size. width*8/10,
                      height:MediaQuery. of(context). size. width*8/10,
                      child: Text(
                          "\n\noops! Something went wrong :(\nPlease try again)",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.5,
                            color: Colors.white,
                          )
                      ),
                    ),
                  );
                }
                List <Classes> classdata=projectSnap.data;
                var numlist=classdata.first.numberlist.trim().split(",");
                var namelist=classdata.first.namelist.trim().split(",");
                print("namelist"+classdata.first.namelist);
                print("numberlist"+classdata.first.numberlist);
                for(int i=0;i<namelist.length-1;i++){
                  print("---------"+namelist.elementAt(i)+"------------\n");
                  print("---------"+numlist.elementAt(i)+"------------\n");
                  //last item after comma is an empty string always
                }
                Future paydata=dbConnect.getPaymentData(classname);
                List <String> payarr=List<String>(numlist.length-1);
                paydata.then((data) {
                  int y=0;
                  for(var x in data) {
                    print("^^^^^^^^^number^^^^" + x.number);
                    String num_i=x.number;
                    String paystat=x.months.toString().split(",").elementAt(currmonthnum==0?currmonthnum:currmonthnum-1).trim();  /////////////////////////////check
                    print("^^^^^^^^^stat^^^^" +paystat);
                    payarr[y]=paystat+","+num_i;
                    y++;
                  }
                }, onError: (e) {
                  print(e);
                });

                List<bool> checked = List.filled(namelist.length-1, false);
                String url;
                String paidlist="";
                return Column(
                  children: [
                    Container(
                        child:Form(
                            key: _formKey,
                            child:
                                  Container(
                                    constraints: BoxConstraints(maxHeight: 200),
                                    margin: EdgeInsets.only(top:10.0,left: 20.0,right: 20.0,bottom: 10.0),
                                    child: TextFormField(
                                       maxLines: 7,
                                      controller: messageController,
                                      autofocus: true,
                                      decoration: const InputDecoration(
                                        icon: const Icon(Icons.school),
                                        hintText: 'Type Message Here',
                                        // enabled: false,
                                      ),
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return 'The Message/Notice Cannot be Empty';
                                        }
                                        return null;
                                      },
                                    ),
                                  ),

                            )

                    ),
                    Visibility(
                      visible:!addnotfromcontact,
                      child: DropdownButton<String>(
                        //items: <String>["Send to Paid", "Send to All", "Send to Custom"].map((String value) {
                        items: <String>["Send to Paid", "Send to All"].map((String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        hint: Text(_selectedtype),
                        onChanged: (newVal) {
                          setState(() {
                            _selectedtype = newVal;
                          });
                          if(_selectedtype=="Send to Custom"){//string compare
                            setState(() {
                              addnotfromcontact=true;
                            });
                          }
                          print("selected : "+_selectedtype);
                        },
                      ),
                    ),
               /*     Expanded(
                      child: Visibility(
                          visible: addnotfromcontact,
                          child:Form(
                            key: _formKey2,
                            child: Container(
                              child: Column(
                                  children: <Widget>[
                                    Text("\nSelect Students to Send the Notice"),
                                    Expanded(
                                      child: ListView.builder(
                                        itemCount: namelist.length-1,
                                        itemBuilder: (context, index) {
                                          return CheckboxListTile(
                                            title: Text('${namelist[index]}'),
                                            subtitle:Text('${numlist[index]}'),
                                            value: checked[index]as bool,
                                            onChanged: (bool newValue) {
                                              setState(() {
                                                checked[index] = !checked.elementAt(index);
                                              });
                                            },
                                            controlAffinity: ListTileControlAffinity.leading,
                                          );
                                        },
                                      ),
                                    ),
                                    FloatingActionButton.extended(
                                      backgroundColor:Colors.blueGrey,
                                      label: Text("Done"),
                                      onPressed:()=>{
                                        print("name "+nameController.text+" number "+numberController.text),
                                        onTapped(numberController.text,nameController.text),
                                        if (_formKey2.currentState.validate()) {
                                          print("validated"),
                                        },
                                        setState(() {
                                          addnotfromcontact=false;
                                        }),
                                      },
                                      heroTag: null,
                                    ),
                                  ]
                              ),
                            ),
                          )
                      ),
                    ),*/
                    Container(
                      margin: EdgeInsets.only(top:10.0,left: 20.0,right: 20.0,bottom: 10.0),
                      child: FloatingActionButton.extended(
                        label: Text("Send via SMS"),
                        onPressed:()async=>{
                          print("message "+messageController.text),
                         // onTapped(numberController.text,nameController.text),
                          setState(() {
                            addnotfromcontact=false;
                          }),
                          if (_formKey.currentState.validate()) {
                            print("validated"),
                            if(_selectedtype=="Send to All"){
                              //send to all in list
                              print("!!!!!!!!! send to all "),
                              url='sms:${classdata.first.numberlist.trim()}?body=${messageController.text}',
                              await launch(url),
                              setState(() {
                                addnotfromcontact=true;
                              }),
                            }else{
                              print("@@@@@@@@@@@@@@@@@@@@@@@@else if"),
                              //send to paid only
                              for(var d in payarr){
                                if(d.split(",").elementAt(0)=="y"){//or last
                                  paidlist=d.split(",").elementAt(1)+","+paidlist,
                                }
                              },
                              if(paidlist==""){
                                print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^empty paid list")
                              }
                              else {
                                print("^^^^^paidlist not empty"),
                                  url =
                                  'sms:${paidlist}?body=${messageController
                                      .text}',
                                  await launch(url),
                                  setState(() {
                                    addnotfromcontact = true;
                                  }),
                                }
                            }
                          },
                        },
                        heroTag:"btn10",
                      ),
                    ),
                  ],
                );
              }
          )

      ),
    );
  }

}
