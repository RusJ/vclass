
import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vclass/main.dart';
import 'package:vclass/model/Meeting.dart';

import 'AddStudents.dart';
import 'DbConnect.dart';
import 'Schedule.dart';

class AddEvent extends StatefulWidget {
  @override
  AddEventState createState() => new AddEventState();
}
class AddEventState extends State<AddEvent>{
  final eventnameController = TextEditingController();

  final _formKey1 = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  DbConnect dbConnect=DbConnect.instance;

  String _selectedtype="allday";
  var _isButtonEnabled = false;
  bool notwholeday=false;
  var fromdate=DateTime.now().toString();
  var todate=DateTime.now().toString();
  bool isallday=false;

  bool is_successfullypushedtodb=false;
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    eventnameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Add New Event")
        ),
        body: Container(
          child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Form(
                  key: _formKey1,
                  child: Container(
                          constraints: BoxConstraints(maxHeight: 200),
                          margin: EdgeInsets.only(left: 15.0,right: 15.0,top: 20.0,bottom: 10.0),
                          child: TextFormField(
                            // maxLines: 2,
                            controller:eventnameController,
                            autofocus: true,
                            decoration: const InputDecoration(
                              icon: const Icon(Icons.meeting_room),
                              hintText: 'Event Description',
                              // enabled: false,
                            ),
                            validator: (value) {

                              if (value.isEmpty) {
                                _isButtonEnabled = false;
                                return 'Please enter valid class name';
                              }
                              /*  if(is_duplicating(value)){
                                _isButtonEnabled = false;
                                return 'You already have a class with this name.\nPlease enter valid class name';
                              }*/

                              _isButtonEnabled = true;
                              return null;
                            },
                          ),
                        ),
                    ),

                     Expanded(
                       child: Column(
                         children: [
                         /*  Visibility(
                                  visible:true,
                                  child:Column(
                                    children: [
                                      ListTile(
                                        title: const Text('All Day'),
                                        leading: Radio(
                                          value: "allday",
                                          groupValue: _selectedtype,
                                          onChanged: (value) {
                                            setState(() {
                                              _selectedtype = value;
                                              notwholeday=false;
                                              isallday=true;
                                            });
                                          },
                                        ),
                                      ),
                                      ListTile(
                                        title: const Text('Add Time'),
                                        leading: Radio(
                                          value: "addtime",
                                          groupValue: _selectedtype,
                                          onChanged: (value) {
                                            setState(() {
                                              _selectedtype = value;
                                              notwholeday=true;
                                              isallday=false;
                                            });
                                          },
                                        ),
                                      ),
                                    ],
                                  )
                                ),*/
                        Container(
                              child: Column(
                                children: [
                                  DateTimePicker(
                                    type: DateTimePickerType.dateTimeSeparate,
                                    dateMask: 'd MMM, yyyy',
                                    initialValue: DateTime.now().toString(),
                                    firstDate: DateTime(2000),
                                    lastDate: DateTime(2100),
                                    icon: Icon(Icons.event),
                                    dateLabelText: 'Date',
                                    timeLabelText: "Time",
                                    onChanged: (val) => {
                                      print(val),
                                      this.setState(() {
                                        fromdate=val;
                                      })
                                    },
                                    validator: (val) {
                                      print(val);
                                      return null;
                                },
                                onSaved: (val) => print(val),
                              ),
                                  DateTimePicker(
                                    type: DateTimePickerType.dateTimeSeparate,
                                    dateMask: 'd MMM, yyyy',
                                    initialValue: DateTime.now().toString(),
                                    firstDate: DateTime(2000),
                                    lastDate: DateTime(2100),
                                    icon: Icon(Icons.event),
                                    dateLabelText: 'Date',
                                    timeLabelText: "Time",

                                    onChanged: (val) =>  {
                                      print(val),
                                      this.setState(() {
                                        todate=val;
                                      })
                                    },
                                    validator: (val) {
                                      print(val);
                                     /* if(fromdate==todate){
                                        return "Please enter a valid time range";
                                      }*/
                                      return null;
                                    },
                                    onSaved: (val) => print(val),
                                  ),
                                ],
                              ),
            padding: EdgeInsets.only(left: 8.0,right: 8.0,top: 20.0,bottom: 20.0),
                            ),
                        Container(
                          child: FloatingActionButton.extended(
                            label: Text("Save"),
                            onPressed:()=>{
                              if (_formKey1.currentState.validate()) {
                                addeventtodb(),
                                Navigator.push(context,
                                    MaterialPageRoute(
                                        builder: (context) => MyApp()
                                    )
                                ),
                               // _showToast(_scaffoldKey.currentContext),
                              },
                            },
                            heroTag: "btn4",
                          ),
                          padding: EdgeInsets.only(top:15.0),
                        ),
                         ],
                       ),
                     ),
              ]
          ),
        ),
    );
  }

  void addeventtodb(){
     print("before insert : "+eventnameController.text+" "+fromdate+" "+todate+" "+isallday.toString());
    Meeting meeting= new Meeting(eventname: eventnameController.text,fromdate: fromdate,todate: todate,isallDay: isallday.toString());
    var val= dbConnect.addCalenderEvent(meeting);
    if(val!=null){//incorrect here..need to check whether val>0
      setState(() {
        is_successfullypushedtodb=true;
      });
    }else{
      is_successfullypushedtodb=false;
    }
   // var newDateTimeObj2 = new DateFormat("dd/MM/yyyy HH:mm:ss").parse("10/02/2000 15:13:09");
  }

  void _showToast(BuildContext context) {

    final scaffold =  _scaffoldKey.currentState;
    String message="";
    if(is_successfullypushedtodb==true){
      message= "Event Added!";
    }else{
      message= "Oops!Something went wrong";
    }
     scaffold.showSnackBar(
      SnackBar(
        content: Text(message),
        backgroundColor: is_successfullypushedtodb?Colors.green:Colors.redAccent,
      ),
    );
  }

}
