class StudentName {
  static const col_id = 'id';
  static const tblStudentName = 'student';
  static const col_mobile = 'mobile';
  static const col_name = 'name';


  StudentName({this.id ,this.name,this.mobile});

  int id;
  String name;
  String mobile;

  StudentName.fromMap(Map<String, dynamic> map) {
    id = map[col_id];
    name = map[col_name];
    mobile = map[col_mobile];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{col_name: name, col_mobile: mobile};
    if (id != null) {
      map[col_id] = id;
    }
    return map;
  }
}