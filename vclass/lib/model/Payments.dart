class Payments {
  static const tblPayments = 'payments';
  static const col_id = 'id';
  static const col_class = 'classname';
  static const col_number= 'mobile';
  static const col_name='name';
  static const col_months='months';


  Payments({this.id ,this.classname,this.number,this.name,this.months});

  int id;
  String classname;
  String number;
  String name;
  String months;

  Payments.fromMap(Map<String, dynamic> map) {
    id = map[col_id];
    classname = map[col_class];
    number = map[col_number];
    name=map[col_name];
    months=map[col_months];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{col_class: classname, col_number: number,col_name:name,col_months:months};
    if (id != null) {
      map[col_id] = id;
    }
    return map;
  }
}