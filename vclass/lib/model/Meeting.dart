import 'dart:ui';

import 'package:flutter/material.dart';

class Meeting {
  static const tblEvents = 'events';
  static const col_id = 'id';
  static const col_event_name = 'eventname';
  static const col_from = 'fromdate';
  static const col_to = 'todate';
  static const col_is_allday='isallday';

  Meeting({this.id,this.eventname, this.fromdate, this.todate,this.isallDay});

  int id;
  String eventname;
  String fromdate;
  String todate;
  String isallDay;

  Meeting.fromMap(Map<String, dynamic> map) {
    id = map[col_id];
    eventname=map[col_event_name];
    fromdate=map[col_from];
    todate=map[col_to];
    isallDay=map[col_is_allday];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{col_event_name: eventname, col_from: fromdate,col_to:todate,col_is_allday:isallDay};
    if (id != null) {
      map[col_id] = id;
    }
    return map;
  }
}