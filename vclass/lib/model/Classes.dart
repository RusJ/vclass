class Classes {
  static const tblClasses = 'classes';
  static const col_id = 'id';
  static const col_name = 'classname';
  static const col_numberlist = 'numberlist';
  static const col_namelist = 'namelist';

  Classes({this.id ,this.classname,this.numberlist,this.namelist});

  int id;
  String classname;
  String numberlist;
  String namelist;

  Classes.fromMap(Map<String, dynamic> map) {
    id = map[col_id];
    classname = map[col_name];
    numberlist = map[col_numberlist];
    namelist=map[col_namelist];
  }

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{col_name: classname, col_numberlist: numberlist,col_namelist:namelist};
    if (id != null) {
      map[col_id] = id;
    }
    return map;
  }
}