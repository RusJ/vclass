
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vclass/model/Classes.dart';

import 'AddStudents.dart';
import 'DbConnect.dart';

class AddnotFromContact extends StatelessWidget {
  final nameController = TextEditingController();
  final numberController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  DbConnect dbConnect=DbConnect.instance;
   String names;
   String numbers;
   String namel;
   String numl;
  // In the constructor, require a Todo.
  AddnotFromContact({Key key}) : super(key: key);

  var _isButtonEnabled = false;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    nameController.dispose();
    numberController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("New Number ")
        ),
        body: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                    children: <Widget>[

                      Container(
                        height:80,
                        constraints: BoxConstraints(maxHeight: 200),
                        margin: EdgeInsets.only(top:40.0,left: 30.0,right: 30.0,bottom: 20.0),
                        child: TextFormField(
                          // maxLines: 2,
                          controller: nameController,
                          autofocus: true,
                          decoration: const InputDecoration(
                            icon: const Icon(Icons.person),
                            hintText: 'Student Name',
                            // enabled: false,
                          ),
                          validator: (value) {

                            if (value.isEmpty) {
                              _isButtonEnabled = false;
                              return 'Please enter a valid name';
                            }
                            _isButtonEnabled = true;
                            return null;
                          },
                        ),
                      ),
                      Container(
                        height:80,
                        constraints: BoxConstraints(maxHeight: 200),
                        margin: EdgeInsets.only(left: 30.0,right: 30.0,top: 0.0,bottom: 30.0),
                        child: TextFormField(
                          // maxLines: 2,
                          controller: numberController,
                          autofocus: true,
                          decoration: const InputDecoration(
                            icon: const Icon(Icons.phone),
                            hintText: 'Mobile phone number',
                            // enabled: false,
                          ),
                          validator: (value) {

                            if (value.isEmpty) {
                              _isButtonEnabled = false;
                              return 'Please enter valid phone number';
                            }
                            _isButtonEnabled = true;
                            return null;
                          },
                        ),
                      ),
                      FloatingActionButton.extended(
                        label: Text("Add Participant"),
                        onPressed:()=>{
                          print("name "+nameController.text+" number "+numberController.text),
                         // onTapped(nameController.text, numberController.text),
                          if (_formKey.currentState.validate()) {
                            print("validated"),
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddStudents())
                            )
                          }
                        },
                      ),
                    ]
                ),
              )
            ]
        )
    );
  }

  void onTapped(String name,String num) {
    print("tapped !"+num);
    //numberlist=s+","+numberlist;
    //print(numberlist);
    numbers=num+","+numbers;
    names=name+","+names;
    print("namelist : "+names);
    print("numberlist : "+numbers);
  }


}
/*
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:vclass/AddnotFromContact.dart';
import 'package:vclass/ShowClasses.dart';
import 'package:vclass/main.dart';
import 'package:vclass/model/Classes.dart';
import 'package:vclass/model/Payments.dart';
import 'DbConnect.dart';

class AddStudents extends StatelessWidget {

  var _isButtonEnabled=false;
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  final myController = TextEditingController();
  final showController = TextEditingController();
  final shownameController = TextEditingController();
  final nameController = TextEditingController();
  final numberController = TextEditingController();

  bool addnotfromcontact=false;

  final String classname;
  AddStudents({Key key, @required this.classname}) : super(key: key);

  DbConnect dbConnect;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    showController.dispose();
    shownameController.dispose();
    nameController.dispose();
    numberController.dispose();
  }
  String numberlist="";
  String namelist="";
  List<Widget>widgetlist = new List();

  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
          title: Text("Add Students")
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[

          Form(
            key: _formKey,
            child :Column(
              children: <Widget>[

              /*  Container(
                  constraints: BoxConstraints(maxHeight: 200),
                  margin:EdgeInsets.all(10.0),
                  child: TextFormField(
                    // maxLines: 2,
                    controller:showController,
                    autofocus: true,
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.list),
                      hintText: 'Participants will auto display here',
                      // enabled: false,
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        _isButtonEnabled=false;
                        return 'Please Add Students to the Class';
                      }
                      _isButtonEnabled=true;
                      return null;
                    },
                  ),
                ),*/
                Container(
                  constraints: BoxConstraints(maxHeight: 200),
                  margin:EdgeInsets.all(10.0),
                  child: TextFormField(
                    // maxLines: 2,
                    controller:shownameController,
                    autofocus: true,
                    decoration: const InputDecoration(
                      icon: const Icon(Icons.list),
                      hintText: 'Participants will auto display here',
                      // enabled: false,
                    ),
                  ),
                ),

              ],
            ),
          ),
          IconButton(
            onPressed: () {
              addnotfromcontact=true;
              print("button status : "+addnotfromcontact.toString());
            },
            color: Colors.blue,
            iconSize:30.0,
            icon: Icon(Icons.add_circle),

          ),
          Container(
            child: Visibility(
              visible: addnotfromcontact,
                child:Form(
                  key: _formKey2,
                  child: Column(
                      children: <Widget>[

                        Container(
                          constraints: BoxConstraints(maxHeight: 200),
                          margin: EdgeInsets.only(top:10.0,left: 20.0,right: 20.0,bottom: 10.0),
                          child: TextFormField(
                            // maxLines: 2,
                            controller: nameController,
                            autofocus: true,
                            decoration: const InputDecoration(
                              icon: const Icon(Icons.person),
                              hintText: 'Student Name',
                              // enabled: false,
                            ),
                            validator: (value) {

                              if (value.isEmpty) {
                                _isButtonEnabled = false;
                                return 'Please enter a valid name';
                              }
                              _isButtonEnabled = true;
                              return null;
                            },
                          ),
                        ),
                        Container(
                          constraints: BoxConstraints(maxHeight: 200),
                          margin: EdgeInsets.only(left: 20.0,right: 20.0,top: 10.0,bottom: 10.0),
                          child: TextFormField(
                            // maxLines: 2,
                            controller: numberController,
                            autofocus: true,
                            decoration: const InputDecoration(
                              icon: const Icon(Icons.phone),
                              hintText: 'Mobile phone number',
                              // enabled: false,
                            ),
                            validator: (value) {

                              if (value.isEmpty) {
                                _isButtonEnabled = false;
                                return 'Please enter valid phone number';
                              }
                              _isButtonEnabled = true;
                              return null;
                            },
                          ),
                        ),
                        FloatingActionButton.extended(
                          label: Text("Add Participant"),
                          onPressed:()=>{
                            print("name "+nameController.text+" number "+numberController.text),
                             onTapped(numberController.text,nameController.text),
                            if (_formKey2.currentState.validate()) {
                              print("validated"),
                            },
                            addnotfromcontact=false,
                          },
                          heroTag: null,
                        ),
                      ]
                  ),
                )
            ),
          ),


          Expanded(
            child: Column(
              children: [
                FutureBuilder(
                    future: loadContacts(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return CircularProgressIndicator();
                      } else {
                        Iterable<Contact> list = snapshot.data;
                        print(list
                            .elementAt(1)
                            .displayName);
                        return Expanded(
                          child: ListView(
                            children:
                            <Widget>[
                              for(var x in list)
                                ListTile(
                                  leading: Icon(Icons.people),
                                  title: Text(
                                      x.displayName
                                  ),
                                  subtitle: Text(
                                      x.phones.isEmpty ? "" : x.phones.first
                                          .value
                                  ),
                                  trailing: IconButton(
                                    icon: Icon(Icons.add_rounded),
                                    // onPressed: AddContact(x.displayName),
                                  ),
                                  onTap: () => onTapped(x.phones.first.value.isEmpty?(x.phones.last.value.isEmpty?"":x.phones.last.value):x.phones.first.value,x.displayName.isEmpty?"":x.displayName),
                                  //onTap: () => onTapped(x),
                                  // tileColor:selected?Colors.red:Colors.transparent,
                                ),

                            ],
                          ),
                        );
                      }
                    }),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(bottom: 10.0),
            //color: Colors.lightBlue,
            child: FloatingActionButton.extended(
              backgroundColor:Colors.deepPurple,
              onPressed:()=>{
                if (_formKey.currentState.validate()) {
                  //classname,numlist to db
                  _formKey.currentState.save(),
                  addClasstoDb(),
                  _formKey.currentState.reset(),
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyApp()),
                  )
                }
              },
              label: Text("  Create Class  "),
            ),
          )
        ],
      ),
    );
  }
  Future<Iterable<Contact>> loadContacts () async {
    print("function called");
    Iterable<Contact> contacts= await ContactsService.getContacts();
    for(var i=0;i<contacts.length;i++){
      print(contacts.elementAt(i).displayName);
    }
    /* Iterable<String> names = contacts.map((c) => c.displayName);
    Iterable<String> numbers = contacts.map((c) => c.phones.first.value);
    print(numbers.elementAt(4));

    List<Widget> list = new List<Widget>();
    for(var i = 0; i < contacts.length; i++){
      list.add(new Text(contacts.elementAt(i).displayName));
    }*/
    return contacts;
  }

  contactOnTapped(var cname) {
    print("Tapped on contact: "+cname);
  }

  void onTapped(String num,String name) {
    print("tapped !"+num);
    //numberlist=s+","+numberlist;
    //print(numberlist);
    showController.text=num+","+showController.text;
    numberlist=showController.text;
    shownameController.text=name+","+shownameController.text;
    namelist=shownameController.text;
   // namelist=name+","+namelist;
    print("namelist : "+namelist);
    print("numberlist : "+numberlist);
  }
 /* void onTapped(Contact c){
    String name=c.displayName;
    String num=c.phones.first.value.isEmpty?c.phones.last.value:c.phones.first.value;
    print("name : "+name+" num :"+num);
    showController.text=num+","+showController.text;
    numberlist=showController.text;
    shownameController.text=name+","+shownameController.text;
    namelist=shownameController.text;
    // namelist=name+","+namelist;
    print("namelist : "+namelist);
    print("numberlist : "+numberlist);

  }*/

  addClasstoDb() {
    print("add class to db invoked");
    dbConnect=DbConnect.instance;
    print("going to store data "+classname+" "+numberlist);
    Classes clz=new Classes(classname: classname,numberlist: numberlist,namelist: namelist);
    dbConnect.addnewclass(clz);

    String new_numlist=numberlist. substring(0, numberlist.length - 1);//remove last comma of numberlist
    List<String>numlist=new_numlist.split(",");
    //List<Payments>pay=[];

    String new_namelist=namelist.substring(0,namelist.length -1);
    List<String>nmlist=new_namelist.split(",");
    int i=0;
    for(var n in numlist){
      print("n :"+n);
      // pay[i]=new Payments(classname: classname,number: n,months: ["n","n","n","n","n","n","n","n","n","n","n","n"]);
      Payments pay=new Payments(classname: classname,number: n,months: "n,n,n,n,n,n,n,n,n,n,n,n");
      dbConnect.addclasspayments(pay);
      i++;
    }

    dbConnect.fetchClasses();
    dbConnect.fetchPayments();
  }

}
 */