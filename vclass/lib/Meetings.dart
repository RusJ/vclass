import 'dart:ui';
import 'package:flutter/material.dart';

class Meetings {
  Meetings(this.eventname, this.from, this.to, this.background, this.isallDay);

  String eventname;

  DateTime from;

  DateTime to;

  Color background=Colors.teal;

  bool isallDay=false;
}