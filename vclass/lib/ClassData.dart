import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:vclass/DbConnect.dart';
import 'package:vclass/model/Classes.dart';
import 'package:vclass/model/Payments.dart';

import 'main.dart';

class ClassData extends StatefulWidget {
  final String classname;
  final int clzID;
  final int listlength;
  ClassData({Key key, @required this.classname,@required this.clzID,@required this.listlength}) : super(key: key);
  @override
  ClassDataState createState() => new ClassDataState(classname: classname,clzID: clzID,listlength:listlength);

}

class ClassDataState extends State<ClassData>{

  Color color;
  int result=0;
  @override
  void initState() {
    super.initState();
    var res=dbConnect.getClassData(classname);
    //isselected=List<bool>.filled(listlength,false);
  }

  final String classname;
  final int clzID;
  final int listlength;
  List<bool> isselected;
  ClassDataState({Key key, @required this.classname,@required this.clzID,@required this.listlength}) {
    classnameController.text = classname;
    isselected=List<bool>.filled(listlength-1,false);
    print("############# length :"+listlength.toString());
  }

  DbConnect dbConnect=DbConnect.instance;
  final _formKey = GlobalKey<FormState>();
  final classnameController = TextEditingController();
  final showController = TextEditingController();
  final numberlistController=TextEditingController();//for newly added names and numbers
  final namelistController=TextEditingController();
  bool addnotfromcontact=false;
  String addorcancelbtn="Add";

  String newliaddednames="";
  String newlyaddednums="";

  var _isButtonEnabled=false;
  var namelist;
  var numlist;
  final _formKey2 = GlobalKey<FormState>();
  final shownameController = TextEditingController();
  final nameController = TextEditingController();
  final numberController = TextEditingController();


  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    showController.dispose();//remove if misbehave
    shownameController.dispose();
    nameController.dispose();
    numberController.dispose();
    numberlistController.dispose();
    namelistController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Widget build(BuildContext context) {
    double height=MediaQuery. of(context). size. height*8/10;
    double width=MediaQuery. of(context). size. width*8/10;
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
            title: Text("Edit/Delete Class")
        ),
        body: Container(
            margin: const EdgeInsets.all(10.0),
            //color: Colors.cyan[600],
            //width: MediaQuery. of(context). size. width*9/10,
          //  height:MediaQuery. of(context). size. height*9/10,
            child: FutureBuilder(
                future: dbConnect.getClassData(classname),
                builder: (context, projectSnap) {
                  if (!projectSnap.hasData) {
                    return Center(
                      child: Container(
                        margin: const EdgeInsets.all(10.0),
                        color: Colors.cyan,
                        width: MediaQuery. of(context). size. width*8/10,
                        height:MediaQuery. of(context). size. width*8/10,
                        child: Text(
                            "\n\noops! Something went wrong :(\nPlease try again)",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.5,
                              color: Colors.white,
                            )
                        ),
                      ),
                    );
                  }
                  List <Classes> classdata=projectSnap.data;
             //     numberlistController.text=classdata.first.numberlist;
               //   namelistController.text=classdata.first.namelist;
                  numlist=classdata.first.numberlist.trim().split(",");
                  namelist=classdata.first.namelist.trim().split(",");
                  print("namelist"+classdata.first.namelist);
                  print("numberlist"+classdata.first.numberlist);
                  for(int i=0;i<namelist.length-1;i++){
                      print("---------"+namelist.elementAt(i)+"------------\n");
                      print("---------"+numlist.elementAt(i)+"------------\n");
                      //last item after comma is an empty string always
                  }

                 return Column(
                   children: [
                     Container(
                         child:Form(
                            key: _formKey,
                            child: Column(
                                 children: <Widget>[
                                   Container(
                                     height: height/8,
                                      constraints: BoxConstraints(maxHeight: 200),
                                      margin: EdgeInsets.only(top:10.0,left: 20.0,right: 20.0,bottom: 10.0),
                                      child: TextFormField(
                                        // maxLines: 2,
                                          controller: classnameController,
                                          autofocus: true,
                                          decoration: const InputDecoration(
                                            icon: const Icon(Icons.school),
                                            hintText: 'Class Name',
                                            // enabled: false,
                                            ),
                                      validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Please enter a valid name';
                                            }
                                            return null;
                                            },
                                      ),
                                    ),
                                  ]
                            )
                         )
                     ),
                     Visibility(
                       visible:!addnotfromcontact,
                       child: IconButton(
                         onPressed: () {
                           //addnotfromcontact=true;
                           print("button status : "+addnotfromcontact.toString());
                           setState(() {
                             addnotfromcontact=true;
                           });
                         },
                         color: Colors.blue,
                         iconSize:30.0,
                         icon: Icon(Icons.add_circle),

                       ),
                     ),
                     Container(
                       child: Visibility(
                           visible: addnotfromcontact,
                           child:Form(
                             key: _formKey2,
                             child: Column(
                                 children: <Widget>[

                                   Container(
                                     height: height/10,
                                     constraints: BoxConstraints(maxHeight: 200),
                                     margin: EdgeInsets.only(top:10.0,left: 20.0,right: 20.0,bottom: 10.0),
                                     child: TextFormField(
                                       // maxLines: 2,
                                       controller: nameController,
                                       autofocus: true,
                                       decoration: const InputDecoration(
                                         icon: const Icon(Icons.person),
                                         hintText: 'Student Name',
                                         // enabled: false,
                                       ),
                                       validator: (value) {

                                         if (value.isEmpty) {
                                           _isButtonEnabled = false;
                                           return 'Please enter a valid name';
                                         }
                                         _isButtonEnabled = true;
                                         return null;
                                       },
                                     ),
                                   ),
                                   Container(
                                     height:height/10,
                                     constraints: BoxConstraints(maxHeight: 200),
                                     margin: EdgeInsets.only(left: 20.0,right: 20.0,top: 10.0,bottom: 10.0),
                                     child: TextFormField(
                                       // maxLines: 2,
                                       controller: numberController,
                                       autofocus: true,
                                       decoration: const InputDecoration(
                                         icon: const Icon(Icons.phone),
                                         hintText: 'Mobile phone number',
                                         // enabled: false,
                                       ),
                                       validator: (value) {

                                         if (value.isEmpty||value=="") {
                                           _isButtonEnabled = false;
                                           return 'Please enter valid phone number';
                                         }
                                         _isButtonEnabled = true;
                                         return null;
                                       },
                                     ),
                                   ),
                                   FloatingActionButton.extended(
                                     label: Text(addorcancelbtn),
                                     onPressed:()=>{
                                       print("name "+nameController.text+" number "+numberController.text),
                                       //onTapped(numberController.text,nameController.text),
                                       if (_formKey2.currentState.validate()) {
                                         print("validated"),

                                         setState(() {
                                           addnotfromcontact = false;
                                           newliaddednames =
                                               nameController.text + "," +
                                                   newliaddednames;
                                           newlyaddednums =
                                               numberController.text + "," +
                                                   newlyaddednums;
                                           //numberlistController.text=numberController.text+","+numberlistController.text;
                                           // namelistController.text=nameController.text+","+namelistController.text;
                                         }),
                                       },
                                       setState(() {
                                         addnotfromcontact = false;
                                       }),

                                       print("---------Newly added name list : "+newliaddednames),
                                       _formKey2.currentState.reset(),
                                     },
                                     heroTag: "btn6",
                                   ),
                                 ]
                             ),
                           )
                       ),
                     ),
                     Expanded(
                       child: ListView.builder(
                        itemCount: namelist.length-1,
                          itemBuilder: (context, index) {
                            return ListTile(
                                title: Text('${namelist[index]}'),
                                subtitle:Text('${numlist[index]}'),
                                leading: Icon(isselected[index]?Icons.delete:Icons.check_box),
                                onTap:(){
                                 // onTapped(numlist[index], namelist[index]);
                                 // isselected[index]=true;
                                  setState(() {
                                    isselected[index]=!isselected[index];
                                   // color = Colors.lightBlueAccent;
                                  });
                                  print("###############################"+isselected[index].toString());
                                  },
                                tileColor: isselected[index]?Colors.lightBlueAccent:Colors.transparent,

                            );

                          },
                       ),
                     ),
                     Container(
                       height: height * 0.25,
                       color: Colors.lightBlue[50],

                         child: ListView.builder(
                           itemCount: newlyaddednums!=""?(newlyaddednums.split(",").length-1):0,
                           itemBuilder: (context, index) {
                             List<String>xxxx=newlyaddednums.split(",");
                             List<String>yyyy=newliaddednames.split(",");
                             return ListTile(
                               title: Text('${yyyy[index]}'),
                               subtitle:Text('${xxxx[index]}'),
                               leading: Icon(Icons.add_circle),
                               tileColor: Colors.blueGrey[100],
                             );

                           },
                         ),
                       ),
                     
                     Row(
                       children: [
                         Container(
                           width: width/2,
                           margin: EdgeInsets.all(5.0),
                           child: FloatingActionButton.extended(
                               label: Text("Update Classs"),
                             onPressed: (){
                                 if(_formKey.currentState.validate()) {
                                   updatewhole();
                                   Navigator.push(
                                     context,
                                     MaterialPageRoute(
                                         builder: (context) => MyApp()),
                                   );
                                 }else{
                                   _showToast(_scaffoldKey.currentContext, 1);
                                 }
                             },
                             heroTag: "btn8",
                           ),
                         ),
                         Container(
                           width: width/2,
                           margin: EdgeInsets.all(5.0),
                           child: FloatingActionButton.extended(
                             label: Text("Delete All"),
                             backgroundColor: Colors.red,
                             onPressed: (){
                               Alert(
                                 closeIcon: Icon(Icons.close),
                                 context: context,
                                 type: AlertType.warning,
                                 title: "Are you sure you want to Delete?",
                                 desc: "This will completely erase PaymentData of this classs",
                                 buttons: [
                                   DialogButton(
                                     child: Text(
                                       "Yes",
                                       style: TextStyle(color: Colors.white, fontSize: 20),
                                     ),
                                     onPressed: () {
                                       deletewhole();
                                       Navigator.of(context, rootNavigator: true).pop();
                                       Navigator.push(
                                         context,
                                         MaterialPageRoute(builder: (context) => MyApp()),
                                       );
                                       if(result>=2){
                                         _showToast(context, 2);
                                       }
                                     },
                                     width: 120,
                                   ),
                                   DialogButton(
                                     child: Text(
                                       "No",
                                       style: TextStyle(color: Colors.white, fontSize: 20),
                                     ),
                                     //onPressed: () => Navigator.pop(context),
                                     onPressed: ()=>Navigator.of(context, rootNavigator: true).pop(),
                                     width: 120,
                                   )
                                 ],
                               ).show();


                             /*  deletewhole();
                               Navigator.push(
                                 context,
                                 MaterialPageRoute(builder: (context) => MyApp()),
                               );
                               if(result>=2){
                                 _showToast(context, 2);
                               }*/
                               // _showToast(_scaffoldKey.currentContext, result);
                             },
                             heroTag: "btn9",
                           ),
                         )
                       ],
                     ),

                   ],
                 );
                }
            )

        ),
    );
  }

  void deletewhole()async{
    print("Delete called");
    int r1=await dbConnect.deleteClasses(clzID);
    if(r1!=null && r1>0){
      setState(() {
        result++;
      });
    }
    int r2=0;
    List<Payments> paylistforclass2=await dbConnect.getPaymentData(classname);
    if(paylistforclass2!=null){
      for (var payrecord in paylistforclass2) { //update all
        int payyid = await dbConnect.get_id_in_paymenttable(payrecord.number, payrecord.name,classname);
        if(payyid!=null) {
         r2=await  dbConnect.deletePayment(payyid);
        }
      }
    }
    if(r2!=null && r2>0){
      setState(() {
        result++;
      });
    }
    print("~~~~~~~~~~~r1 r2 result~~~~~~~`"+r1.toString()+" "+r2.toString()+" "+result.toString());
    //_showToast(_scaffoldKey.currentContext, result);
  }

  void updatewhole(){
    String newclz=classnameController.text.trim();
    int i=0;
    String newnames="";
    String newnums="";
    int countnums=0;
    for(var y in isselected){
      print("########"+y.toString());
      if(!y){//not selected
        newnames=newnames+","+namelist[i];//////////////////////wrong
        newnums=newnums+","+numlist[i];
      }else{
        countnums++;
      }
      print("newnames "+newnames);
      print("newnums"+newnums);
      i++;
    }
    if(newnums==""||countnums==newnums.split(",").length-1){////check
      //call toast
      _showToast(context, 3);
    }
    newnames=newnames.substring(1,newnames.length);
    newnums=newnums.substring(1,newnums.length);

    newnames=newnames+",";
    newnums=newnums+",";
    print("\n!!!!!!!!!!!!"+"\n!!!!!!new!!!"+newnames);
    if(newliaddednames!=""&&newlyaddednums!=""){
      List<String> newwwnames=newliaddednames.split(",");
      List<String> newwwnums=newlyaddednums.split(",");
      if(newnums!="") {//remove last comma of namelist and nimlist
        newnames = newnames.substring(0, newnames.length-1);
        newnums = newnums.substring(0, newnums.length-1);
      }
      for(int z=0;z<newwwnums.length-1;z++) {//add newly added
        newnames =newnames+","+ newwwnames.elementAt(z) ;
        newnums = newnums+","+newwwnums.elementAt(z);
      }
      //add comma at the end of list
      newnames = newnames + ",";
      newnums = newnums + ",";

    }
    updateClassinDb(newnames,newnums,newclz);
  }

  updateClassinDb(String newnames,String newnums,String newclass) {
    print("add class to db invoked");
    dbConnect = DbConnect.instance;
    print("going to store data " + newclass + " " + newnames +
        " " + newnums);
    Classes clz = new Classes(
        id: clzID, classname: newclass, numberlist: newnums, namelist: newnames);
    dbConnect.updateClasses(clz);

    //change classname in paymnet table
    updatePaymnetsinDb(newclass);
  }
  updatePaymnetsinDb(String newclaass)async{//get changed classname, removed number and delete those numbers, added newly added number

    List<Payments> paylistforclass=await dbConnect.getPaymentData(classname);
    for(var payrecord in paylistforclass){//update all
      Payments payy=new Payments(classname: newclaass,number: payrecord.number,name: payrecord.name,months: payrecord.months);
      dbConnect.updatePaymentofClass(payrecord.number,payrecord.name,classname,payy);
    }

    int place=0; //delete
    int r3;
    for(var yy in isselected){
      print("########"+yy.toString());
      if(yy){// selected
        String tempname=paylistforclass.elementAt(place).name;
        String tempnum=paylistforclass.elementAt(place).number;
        print("before get#########"+tempname+" "+tempnum+" "+newclaass);
        int idd=await dbConnect.get_id_in_paymenttable(tempnum, tempname,newclaass);
        if(idd!=null) {
          print("delete id "+idd.toString());
          r3=await dbConnect.deletePayment(idd);
        }
      }
      place++;
    }

    if(newliaddednames!=""&&newlyaddednums!=""){
      List<String> newwwnames2=newliaddednames.split(",");
      List<String> newwwnums2=newlyaddednums.split(",");
      print("!!!!!!!!!!!!!newnameslength "+newwwnums2.length.toString()+"  ##"+newlyaddednums);
      for(int i=0;i<newwwnums2.length-1;i++){
        Payments pay2=new Payments(classname: newclaass,number: newwwnums2.elementAt(i),name: newwwnames2.elementAt(i),months: "n,n,n,n,n,n,n,n,n,n,n,n");
        dbConnect.addclasspayments(pay2);
        }
      }

  }

  void _showToast(BuildContext context,int condition) {
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!toast!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    final scaffold = _scaffoldKey.currentState;
    String message="";
    if(condition==2){
      message= "Success!";
    }else if(condition==3) {
      message="A class should have at least one Existing Member";
    }else{
      message="Oops!Something went wrong";
    }
    scaffold.showSnackBar(
      SnackBar(
        content: Text(message),
        action: SnackBarAction(
            label: 'Hide', onPressed: scaffold.hideCurrentSnackBar),
        backgroundColor: condition==2?Colors.green:Colors.redAccent,
      ),
    );
  }

}
