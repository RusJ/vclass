
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:vclass/model/Classes.dart';

import 'AddStudents.dart';
import 'DbConnect.dart';

class AddClass extends StatefulWidget {

  final String classname;
  // In the constructor, require a Todo.
  AddClass({Key key, @required this.classname}) : super(key: key);

  AddClassState createState()=>new AddClassState(classname: classname);
}

class AddClassState extends State<AddClass>{

  final myController = TextEditingController();
  final checkreplicationController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  DbConnect dbConnect=DbConnect.instance;
  final String classname;

  // In the constructor, require a Todo.
  AddClassState({Key key, @required this.classname}) {
    checkreplicationController.text="yes";
  }
  bool isreplicated=false;
  var _isButtonEnabled = false;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    checkreplicationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("New Class")
        ),
        body: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Form(
                key: _formKey,
                child: Column(
                    children: <Widget>[

                      Container(
                        height:80,
                        constraints: BoxConstraints(maxHeight: 200),
                        margin: EdgeInsets.all(20.0),
                        child: TextFormField(
                          // maxLines: 2,
                          controller: myController,
                          autofocus: true,
                          decoration: const InputDecoration(
                            icon: const Icon(Icons.school),
                            hintText: 'Add a Unique Class Name',
                            // enabled: false,
                          ),
                          validator: (value) {
                           // dbConnect.is_replicatedclass(value).then((res) =>{checkreplicationController.text=res});
                           // print("checkreplicatedcontroller "+checkreplicationController.text);

                            if (value.isEmpty) {
                              setState(() {
                                _isButtonEnabled=false;
                              });
                              return 'Please enter valid class name';
                            }
                            /*checkreplication(myController.text);
                            if(isreplicated){
                              setState(() {
                                _isButtonEnabled=false;
                              });
                              return "Class with this name already exists..\nPlease give a unique class name";
                            }*/
                          /*  if(is_duplicating(value)){
                              _isButtonEnabled = false;
                              return 'You already have a class with this name.\nPlease enter valid class name';
                            }*/

                            setState(() {
                              _isButtonEnabled=true;
                            });
                            return null;
                          },
                        ),
                      ),
                      FloatingActionButton.extended(
                          label: Text("Add Participants"),
                          //backgroundColor: _isButtonEnabled?Colors.blue:Colors.grey,
                          onPressed:()=>{
                           // checkreplication(myController.text),
                          if (_formKey.currentState.validate()) {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => AddStudents(classname:myController.text))
                              )
                            }
                          },
                        heroTag: "btn7",
                      ),
                    ]
                ),
              )
            ]
        )
    );
  }

checkreplication(String value)async{

    int cnt=await dbConnect.getclasscount(value.trim());
    print("####count : "+cnt.toString());
    if(cnt==0){
      this.setState(() {
        isreplicated=false;
      });
    }else{
      this.setState(() {
        isreplicated=true;
      });
    }
}

}
