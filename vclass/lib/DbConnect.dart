import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:vclass/model/Meeting.dart';
import 'package:vclass/model/Payments.dart';
import 'model/Classes.dart';

class DbConnect{
  static const _databaseName = 'vclass.db';
  static const _databaseVersion = 1;

  DbConnect._();
  static final DbConnect instance = DbConnect._();

  Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await createDatabase();
    return _database;
  }

  createDatabase() async {
    Directory dataDirectory = await getApplicationDocumentsDirectory();
    String dbPath = join(dataDirectory.path, _databaseName);
    print(dbPath);
    return await openDatabase(dbPath,
        version: _databaseVersion, onCreate: onCreateDB);
  }
  Future onCreateDB(Database db, int version) async {
    //create tables
    await db.execute('''
      CREATE TABLE ${Classes.tblClasses}(
        ${Classes.col_id} INTEGER PRIMARY KEY AUTOINCREMENT,
        ${Classes.col_name} TEXT NOT NULL,
        ${Classes.col_numberlist} TEXT NOT NULL,
        ${Classes.col_namelist} TEXT NOT NULL
      )      
      ''');
    await db.execute('''
        CREATE TABLE ${Payments.tblPayments}(
    ${Payments.col_id} INTEGER PRIMARY KEY AUTOINCREMENT,
        ${Payments.col_class} TEXT NOT NULL,
        ${Payments.col_number} TEXT NOT NULL,
        ${Payments.col_name} TEXT NOT NULL,
        ${Payments.col_months} TEXT NOT NULL
    )
    ''');
    await db.execute('''
        CREATE TABLE ${Meeting.tblEvents}(
        ${Meeting.col_id} INTEGER PRIMARY KEY AUTOINCREMENT,
        ${Meeting.col_event_name} TEXT NOT NULL,
        ${Meeting.col_from} TEXT NOT NULL,
        ${Meeting.col_to} TEXT NOT NULL,
        ${Meeting.col_is_allday} TEXT NOT NULL
    )
    ''');
  }

  Future<int> addnewclass(Classes clz) async {
    Database db = await database;

    return await db.insert(Classes.tblClasses, clz.toMap());
  }

  Future<int> addclasspayments(Payments pay) async {
    Database db = await database;

    return await db.insert(Payments.tblPayments, pay.toMap());
  }

  Future<int> addCalenderEvent(Meeting meeting) async {
    Database db = await database;
    return await db.insert(Meeting.tblEvents,meeting.toMap());
  }

  Future<List<Classes>> fetchClasses() async {
    Database db = await database;
    List<Map> clzes = await db.query(Classes.tblClasses);
    print("clzes : "+clzes.toString());
    return clzes.length == 0
        ? []
        : clzes.map((x) => Classes.fromMap(x)).toList();
  }

  Future<List<Payments>> fetchPayments() async {
    Database db = await database;
    List<Map> pays = await db.query(Payments.tblPayments);
    print("pays : "+pays.toString());
    return pays.length == 0
        ? []
        : pays.map((x) => Payments.fromMap(x)).toList();//Classes was there instead of Payments
  }

  Future<List<Meeting>> fetchEvents() async {
    Database db = await database;
    List<Map> events = await db.query(Meeting.tblEvents);
    print("events : "+events.toString());
    return events.length == 0
        ? []
        : events.map((x) => Meeting.fromMap(x)).toList();
  }

  Future<int> updateClasses(Classes clz) async {
    Database db = await database;
    return await db.update(Classes.tblClasses, clz.toMap(),
        where: '${Classes.col_id}=?', whereArgs: [clz.id]);
  }
//contact - delete
  Future<int> deleteClasses(int id) async {
    Database db = await database;
    return await db.delete(Classes.tblClasses,
        where: '${Classes.col_id}=?', whereArgs: [id]);
  }

  Future<int> updatePayment(Payments pay) async {
    Database db = await database;
    final res = await db.query(
        Payments.tblPayments,
        where:Payments.col_number+" = (?) AND "+Payments.col_name+" = (?) AND "+Payments.col_class+" = (?)",
        whereArgs: [pay.number,pay.name,pay.classname],
    );
    var id=res.elementAt(0)['id'];
    print("idddd "+id.toString());
    return await db.update(Payments.tblPayments, pay.toMap(),
        where: '${Payments.col_id}=?', whereArgs: [id]);
  }

  Future<int> updatePaymentofClass(String num,String nm,String clz,Payments pay) async {
    String clz_=clz.trim().toLowerCase();
    Database db = await database;
    final res = await db.query(
      Payments.tblPayments,
      where:Payments.col_number+" = (?) AND "+Payments.col_name+" = (?) AND "+Payments.col_class+" = (?)",
      whereArgs: [num,nm,clz_],
    );
    var id=res.elementAt(0)['id'];
    print("idddd "+id.toString());
    return await db.update(Payments.tblPayments, pay.toMap(),
        where: '${Payments.col_id}=?', whereArgs: [id]);
  }
  Future<int> get_id_in_paymenttable(String num,String nm,String clz) async {
    String classz=clz.trim().toLowerCase();
    Database db = await database;
    final res = await db.query(
      Payments.tblPayments,
      where:Payments.col_number+" = (?) AND "+Payments.col_name+" = (?) AND "+Payments.col_class+" = (?)",
      whereArgs: [num,nm,classz],
    );
    var id=null;
    if(res!=null) {
       id = res.elementAt(0)['id'];
    }
    print("iddddingetpaytable " + id.toString());
    return id;
  }

//contact - delete
  Future<int> deletePayment(int id) async {
    Database db = await database;
    return await db.delete(Payments.tblPayments,
        where: '${Payments.col_id}=?', whereArgs: [id]);
  }

  Future<int> updateEvent(Meeting meeting) async {
    Database db = await database;
    return await db.update(Meeting.tblEvents, meeting.toMap(),
        where: '${Meeting.col_id}=?', whereArgs: [meeting.id]);
  }

  Future<int> deleteEvent(int id) async {
    Database db = await database;
    return await db.delete(Meeting.tblEvents,
        where: '${Meeting.col_id}=?', whereArgs: [id]);
  }

  Future<String> is_replicatedclass(String value) async {
    Database db = await database;
    final res = await db.query(
      Classes.tblClasses,
      where: Classes.col_name.toLowerCase()+" = (?)",
      whereArgs: [value.toLowerCase()],
      limit: 1
    );
    if(res.length!=0){
      return("yes");
    }else{
      return("no");
    }
    /*if(res.length!=0) {
      for (var s in res) {
        print(s);
      }
    }*/
  }

  getAllclassnames() async{
      Database db = await database;
      List<Map> res = await db.query(
          Classes.tblClasses,
          columns: [Classes.col_name]
      );
      print(res);
      return res.map((x) =>x.toString());
  }

  getClassData(String value) async{
    Database db = await database;
    List<Map> clz = await db.query(
        Classes.tblClasses,
        where: Classes.col_name.toLowerCase()+" = (?)",
        whereArgs: [value.toLowerCase()],
    );
    print("clz : "+clz.toString());
    return clz.length == 0
        ? []
        : clz.map((x) => Classes.fromMap(x)).toList();
  }

  getPaymentData(String value) async{
    print(pid);
    Database db = await database;
    List<Map> pay = await db.query(
      Payments.tblPayments,
      where: Payments.col_class.toLowerCase()+" = (?)",
      whereArgs: [value.toLowerCase()],
    );
    print("pay : "+pay.toString());
    return pay.length == 0
        ? []
        : pay.map((x) => Payments.fromMap(x)).toList();
  }

  Future<int> getclasscount(String name)async{
    Database db=await database;
    var x=await db.query(Classes.tblClasses,
      where: Classes.col_name.toLowerCase()+" = (?)",
      whereArgs: [name.toLowerCase()],
    );
   // int count = Sqflite.firstIntValue(x);
    return x.length;
  }



}